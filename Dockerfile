FROM ruby:2.4.1-alpine
ADD ./ /app/
WORKDIR /app
ENV PORT 5000
EXPOSE 5000

RUN chmod +x /app/entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["web"]
