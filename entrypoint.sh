#!/usr/bin/env sh

case "$1" in
  web)
    ruby ./server.rb
    ;;
  worker)
    ruby ./server.rb & # for the liveness probes
    i=1
    while : ; do
      echo "Worker command $i"
      i=$(( i + 1 ))
      sleep 5
    done
    ;;
esac
